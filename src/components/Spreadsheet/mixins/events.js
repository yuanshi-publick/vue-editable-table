/**
 * @file 提供发出事件的方法
 *
 * @author: 元实
 */

/**
 * 1. 2021-03-01 元实 初始版本
 */

export default {
    methods: {
      /**
       * 把事件发出到所有的父组件
       *
       * @param {string} name  事件名称
       * @param {*} [params=null]
       * @param {Object} options
       */
      broadcastEvent(name, params=null, options) {
        let parent = this.$parent;
        let component = this;
  
        while (parent) {
          component.$emit(name, params, options);
          component = parent;
          parent = parent.$parent;
        }
      },
    },
  };
  
  